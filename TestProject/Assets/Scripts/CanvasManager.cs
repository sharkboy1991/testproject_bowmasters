﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour
{
    public Image[] healthBars;
    public Image[] playerIcon;


    public void SetIcon(int index, Sprite icon) 
    {
        playerIcon[index].sprite = icon; 
    }

    public void SetHealth(int index, float health)
    {
        healthBars[index].fillAmount = Mathf.Lerp(1f, 0f, (health - 100f) / -100f);

        if (healthBars[index].fillAmount <= 0f)
            Invoke("ReturnToMainMenu", 3f);
    } 

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
