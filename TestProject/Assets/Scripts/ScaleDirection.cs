﻿using UnityEngine;

public class ScaleDirection : MonoBehaviour
{
    public Vector3 leftScale = new Vector3(-1,1,1);
    public Vector3 rightScale = new Vector3(1, 1, 1);

    public void SetScaleDirection(bool isRightScale) 
    {
        if (isRightScale)
        {
            transform.localScale = rightScale;
        }
        else
        {
            transform.localScale = leftScale;
        }
    }
}
