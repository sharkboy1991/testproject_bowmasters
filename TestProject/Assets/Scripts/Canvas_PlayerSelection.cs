﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Canvas_PlayerSelection : MonoBehaviour
{
    [Header("CHARACTER PREVIEW")]
    public CharacterPreview _charPreview;
    public Transform socket_hand_l;
    public Transform socket_hand_r;

    [Header("PLAYER SELECTION")]
    public int playerSkinIndex = 0;
    public GameObject psb_prefab;
    public Transform ps_layout;
    public int maxPlayersSkins;
    public Color[] ps_colors;
    private PlayerSelectionButton[] ps_buttons;

    [Header("WEAPON TYPE")]
    public Image weapIcon;
    public Text weapText;
    public Sprite[] weapSprites;



    void Start()
    {
        ps_buttons = new PlayerSelectionButton[maxPlayersSkins];
        CreatePlayerSelectionButtons();
    }

    private void CreatePlayerSelectionButtons() 
    {
        for (int i=0; i<maxPlayersSkins; i++) 
        {
            GameObject clone = Instantiate(psb_prefab);
            clone.transform.SetParent(ps_layout);
            clone.transform.localPosition = Vector3.zero;
            clone.transform.localEulerAngles = Vector3.zero;
            clone.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);

            Sprite[] sprite = Resources.LoadAll<Sprite>("Character_Skins/CC_Skin_" + i);

            ps_buttons[i] = clone.GetComponent<PlayerSelectionButton>();
            ps_buttons[i].InitButton(this, i, sprite[0]);
        }

        playerSkinIndex = 0;
        SelectPlayer(playerSkinIndex);
    }

    public void SelectPlayer(int index)
    {
        Sprite[] sprite = Resources.LoadAll<Sprite>("Character_Skins/CC_Skin_" + index);
        _charPreview.SetCharacterSkinPreview(sprite);

        ps_layout.localPosition = new Vector3(-(280f * index), 0, 0);

        ps_buttons[playerSkinIndex].SetBoardColor(ps_colors[0]);
        ps_buttons[playerSkinIndex].transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);

        playerSkinIndex = index;

        ps_buttons[playerSkinIndex].SetBoardColor(ps_colors[1]);
        ps_buttons[playerSkinIndex].transform.localScale = new Vector3(1f, 1f, 1f);


        //delete old weapons
        if (socket_hand_l.childCount != 0)
            Destroy(socket_hand_l.GetChild(0).gameObject);

        if (socket_hand_r.childCount != 0)
            Destroy(socket_hand_r.GetChild(0).gameObject);

        //add new weapon
        GameObject weapon = Instantiate( Resources.Load<GameObject>("Character_Weapons/CC_Weapon_" + index.ToString("000")));
        WeaponController weapController = weapon.GetComponent<WeaponController>();
        if (weapController.weaponType == WeaponController.WeaponType.Bow)
            weapon.transform.SetParent(socket_hand_l);
        else if (weapController.weaponType == WeaponController.WeaponType.Spear)
            weapon.transform.SetParent(socket_hand_r);
        weapon.transform.localPosition = Vector3.zero;
        weapon.transform.localEulerAngles = Vector3.zero;
        weapon.transform.localScale = Vector3.one;

        //update weapon type icon
        weapIcon.sprite = weapSprites[(int)weapController.weaponType];
        weapIcon.SetNativeSize();

        //update weapon text info
        if ((int)weapController.weaponType == 0)
            weapText.text = "BOW";
        else if ((int)weapController.weaponType == 1)
            weapText.text = "SPEAR";
    }

    public void Play()
    {
        PlayerPrefs.SetInt("PlayerSkinSelection", playerSkinIndex);
        SceneManager.LoadScene(1);
    }
}
