﻿using UnityEngine;

public class CharacterPreview : MonoBehaviour
{
    public SpriteRenderer[] sprites;

    public void SetCharacterSkinPreview(Sprite[] spriteSkin)
    {
        sprites[0].sprite = spriteSkin[1];//body sprite
        sprites[1].sprite = spriteSkin[0];//head sprite
        sprites[2].sprite = spriteSkin[2];//arm R sprite
        sprites[3].sprite = spriteSkin[3];//lower R arm sprite
        sprites[4].sprite = spriteSkin[2];//arm L sprite
        sprites[5].sprite = spriteSkin[4];//lower L arm sprite
        sprites[6].sprite = spriteSkin[5];//thump sprite
        sprites[7].sprite = spriteSkin[6];//leg R sprite
        sprites[8].sprite = spriteSkin[7];//lower leg R sprite
        sprites[9].sprite = spriteSkin[6];//leg L sprite
        sprites[10].sprite = spriteSkin[7];//lower leg L sprite
    }
}
