﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class CameraController : MonoBehaviour
{
    private Camera _cam;
    private GameManager _gameMng;

    private Transform playerTfm, enemyTfm, projectileTfm;

    public CameraState _camState;
    public enum CameraState { Freezed, MoveToP1, MoveToP2, FolowProjectile};
    private Vector3 camPos;
    private float dis;
    private bool reachedTarget;

    public ZoomEffect _zoomEffect;
    [System.Serializable] public class ZoomEffect
    {
        public int steps;
        public float waitTime;
        public AnimationCurve timeCurve;

        public UnityEvent onEffectStart;
        public UnityEvent onEffectEnd;
    }





    public void InitCameraController(GameManager gameMng, Transform p1, Transform p2) 
    {
        _gameMng = gameMng;

        _cam = GetComponent<Camera>();
        _cam.orthographicSize = 5f;

        _camState = CameraState.Freezed;

        playerTfm = p1;
        enemyTfm = p2;

        camPos.x = enemyTfm.position.x;
        camPos.y = enemyTfm.position.y;
        camPos.z = -10;

        transform.position = camPos;
        StartCoroutine(ZoomTarget(false));
    }

    public void FollowProjectile(Transform projectile) 
    {
        projectileTfm = projectile;
        _camState = CameraState.FolowProjectile;
    }

    public void FollowPlayer() 
    {
        _camState = CameraState.MoveToP1;
        reachedTarget = false;
    }

    public void FollowEnemy()
    {
        _camState = CameraState.MoveToP2;
        reachedTarget = false;
    }

    private void Update()
    {
        if (_camState == CameraState.MoveToP1)
        {
            camPos = playerTfm.position;
            camPos.z = -10;

            transform.position = Vector3.Lerp(transform.position, camPos, Time.deltaTime * 1.5f);

            if (!reachedTarget)
            {
                dis = Vector3.Distance(transform.position, camPos);
                if (dis <= 5f)
                {
                    reachedTarget = true;
                    StartCoroutine( ZoomTarget(true));
                }
            }
        }
        else if (_camState == CameraState.MoveToP2)
        {
            camPos = enemyTfm.position;
            camPos.z = -10;

            transform.position = Vector3.Lerp(transform.position, camPos, Time.deltaTime * 1.5f);

            if (!reachedTarget)
            {
                dis = Vector3.Distance(transform.position, camPos);
                if (dis <= 5f)
                {
                    reachedTarget = true;
                    StartCoroutine(ZoomTarget(true));
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (_camState == CameraState.FolowProjectile)
        {
            camPos = projectileTfm.position;
            camPos.z = -10;

            //transform.position = camPos;
            transform.position = Vector3.Lerp(transform.position, camPos, Time.deltaTime * 15f);
        }
    }

    IEnumerator ZoomTarget(bool attackState)
    {
        //invoke event
        _zoomEffect.onEffectStart.Invoke();

        //simulate camera animation
        float amount = (_cam.orthographicSize - 4f) / (float)_zoomEffect.steps;
        float value = _cam.orthographicSize;
        for (int i = 0; i < _zoomEffect.steps; i++)
        {
            value -= amount;
            _cam.orthographicSize = value;
            yield return new WaitForSeconds(_zoomEffect.waitTime * _zoomEffect.timeCurve.Evaluate(value));
        }


        if (attackState)
            _gameMng.HandleRound(attackState);
        else
            FollowPlayer();

        //invoke event
        _zoomEffect.onEffectEnd.Invoke();
    }
}
