﻿using System.Collections;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public WeaponType weaponType;
    public enum WeaponType { Bow, Spear }

    [Header("WEAPON OPTIONS")]
    public Transform projectilePosition;
    public GameObject projectilePref;
    private GameObject spawnedProjectile;
    public SpriteRenderer weaponRenderer;
    public Sprite projectileSprite;

    private ProjectileController _projectileCtrl;






    public void SpawnProjectile(Transform parent)
    {
        spawnedProjectile = Instantiate(projectilePref);
        spawnedProjectile.transform.SetParent(parent);
        
        spawnedProjectile.transform.localScale = Vector3.one;

        if (weaponType == WeaponType.Bow)
        {
            spawnedProjectile.transform.position = projectilePosition.position;
            spawnedProjectile.transform.localEulerAngles = Vector3.zero;
        }
        else if (weaponType == WeaponType.Spear)
        {
            spawnedProjectile.transform.localPosition = Vector3.zero;
            spawnedProjectile.transform.localEulerAngles = new Vector3(0, 0, 90f);
        }

        _projectileCtrl = spawnedProjectile.GetComponent<ProjectileController>();
        _projectileCtrl.SetProjectileSprite(projectileSprite);
    }

    public Transform Shoot(Vector3 shootPos, Quaternion shootRot, float power) 
    {
        spawnedProjectile.transform.SetParent(null);
        spawnedProjectile.transform.position = shootPos;
        spawnedProjectile.transform.rotation = shootRot;

        _projectileCtrl.ShootProjectile(power, 0);

        if (weaponType == WeaponType.Bow)
        {
            //bow stuffs
        }
        else if (weaponType == WeaponType.Spear)
        {
            weaponRenderer.enabled = false;
            StartCoroutine(ShowWeapon());
        }

        return spawnedProjectile.transform;
    }

    IEnumerator ShowWeapon() 
    {
        yield return new WaitForSeconds(2f);
        weaponRenderer.enabled = true;
    }
}
