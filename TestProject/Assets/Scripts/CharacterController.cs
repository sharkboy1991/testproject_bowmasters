﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;


public class CharacterController : MonoBehaviour
{
    //VARIABLES
    #region REGION
    [System.Serializable] public class Sockets 
    {
        public Transform hand_l;
        public Transform hand_r;
        public Transform projectile;
    }

    [System.Serializable] public class DragAimSystem 
    {
        public bool isFacingRight;
        public bool isAiming;
        public Transform aimPivot;
        public GameObject aimDragUi;
        public GameObject mouseDragUi;
        public Vector3 clickPosition;
        public Vector3 mousePosition;
        public float dragDistance;

        [Header("OPPONENT DISTANCE BOX")]
        public Transform disBox;
        public Transform disBoxDir;
        public TextMeshPro disText;
        public SpriteRenderer disCharSprite;

        [Header("AIM VALUE")]
        [Range(0, 5)] public float aimMaxDragDistance;
        [Range(0, 100)] public float aimPower;
        [Range(-90, 90)] public float aimAngle;
        public TextMeshPro aimPowerText, aimAngleText;

        [Header("AIM DOTS")]
        public Transform[] dotsPivots;
        public GameObject dot;
        public int dotsLength;
        public float dotsSpace;
    }

    [System.Serializable] public class Ai 
    {
        public bool isAI;
    }

    [System.Serializable] public class CharacterRig 
    {
        public bool ragdollState = false;
        public Animator _anim;
        public Transform[] rigPivot;
        public SpriteRenderer[] sprites;
        public LayerMask ragdollMask;

        [Header ("RIG GIZMOS")]
        public bool debugGizmos;
        [Range(0.01f, 0.1f)] public float gizmosScale = 0.05f;
    }

    [System.Serializable] public class Events
    {
        public UnityEvent OnStart;
        public UnityEvent OnActivateAiming;
        public UnityEvent OnStartAiming;
        public UnityEvent OnShoot;
        public UnityEvent OnGetHit;
        public UnityEvent OnFlipRight;
        public UnityEvent OnFlipLeft;
    }


    public Sockets _sockets;
    public DragAimSystem _aim;
    public Ai _ai;
    public CharacterRig _characterRig;
    public Events _events;

    private int ccIndex;//character controller index

    private GameManager _gameMng;
    private WeaponController _weap;

    private Camera _cam;
    private GameObject[] dots;
    private GameObject dotsGroup, aimDragClone, mouseDragClone;
    private Transform projectile;

    private bool controlState, aiAimIsReady, aiPowerIsReady;

    private float health, shotPower, angle, aiAngle, aiPower;
    private Vector2 direction;


    //RAGDOLL
    private HingeJoint2D[] rd_joints;
    private Rigidbody2D[] rd_rigs;
    private Dictionary<Rigidbody2D, Vector3> rd_initPos = new Dictionary<Rigidbody2D, Vector3>();
    private Dictionary<Rigidbody2D, Quaternion> rd_initRot = new Dictionary<Rigidbody2D, Quaternion>();
    #endregion


    //MAIN METHODS
    #region REGION
    private void Awake()
    {
        //find main camera
        _cam = Camera.main;

        //set ai dots ui length
        dots = new GameObject[_aim.dotsLength];

        //setup ragdoll
        rd_rigs = GetComponentsInChildren<Rigidbody2D>();
        rd_joints = GetComponentsInChildren<HingeJoint2D>();
        foreach (Rigidbody2D rig in rd_rigs)
        {
            rd_initPos.Add(rig, rig.transform.localPosition);
            rd_initRot.Add(rig, rig.transform.localRotation);
        }
        DeactivateRagdoll();
    }

    private void Start() 
    {
        _events.OnStart.Invoke();
    }

    public void InitCharacterController(int controllerIndex, GameManager gameManager, bool facingRight, bool isAi) 
    {
        ccIndex = controllerIndex; //character controller index
        _gameMng = gameManager;  //game manager script

        _ai.isAI = isAi;
        health = 100f;

        SetFacingDirection(facingRight); //controller facing direction
    }

    private void Update()
    {
        if (!controlState)
            return;

        if (_ai.isAI)
            AiController();//ai control
        else
        {
#if UNITY_EDITOR
            PcController();//pc control
#elif UNITY_ANDROID
            MobileController();//mobile
#endif
        }
    }

    private void OnDrawGizmos()
    {
        if (_characterRig.debugGizmos)
        {
            for (int i = 0; i < _characterRig.rigPivot.Length; i++)
                Gizmos.DrawWireSphere(_characterRig.rigPivot[i].position, _characterRig.gizmosScale);
        }
    }
    #endregion


    //PLAYER CONTROLLER
    #region REGION
    private void PcController()
    {
        if (Input.GetMouseButtonDown(0))
            PlayerStartAiming();

        if (Input.GetMouseButtonUp(0))
            PlayerStopAiming();

        if (_aim.isAiming)
            PlayerAiming(Input.mousePosition);

        //update camera zoom
        SetUpdateDistanceBoxPosition();
    }

    private void MobileController()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
                PlayerStartAiming();

            if (touch.phase == TouchPhase.Ended)
                PlayerStopAiming();

            if (_aim.isAiming)
                PlayerAiming(touch.position);
        }

        //update camera zoom
        SetUpdateDistanceBoxPosition();
    }

    private void PlayerStartAiming() 
    {
        if (!GetScreenAimBounds((Vector2)Input.mousePosition))
            return;

        _aim.isAiming = true;

        //setup opponent distance popup ui
        SetUpdateDistanceBoxPosition();
        _aim.disText.text = Vector3.Distance(transform.position, _gameMng.GetOpponentTranform(ccIndex).position).ToString("0") + "M";


        //update aiming main variables
        if (_weap.weaponType == WeaponController.WeaponType.Bow)
            _weap.SpawnProjectile(_sockets.hand_l);
        else if (_weap.weaponType == WeaponController.WeaponType.Spear)
            _weap.SpawnProjectile(_sockets.hand_r);

        //turn screen's clicked position to world space
        _aim.clickPosition = _cam.ScreenToWorldPoint(Input.mousePosition);

        //update animator variables
        _characterRig._anim.SetInteger("WeaponType", (int)_weap.weaponType);
        _characterRig._anim.SetBool("IsAiming", _aim.isAiming);

        //call event
        _events.OnStartAiming.Invoke();

        //destroy old dots group
        if (dotsGroup != null)
            Destroy(dotsGroup);

        //create new dots group
        dotsGroup = new GameObject();
        dotsGroup.transform.position = Vector3.zero;
        dotsGroup.transform.eulerAngles = Vector3.zero;
        dotsGroup.transform.localScale = Vector3.one;

        //spawn aim dots
        for (int i = 0; i < _aim.dotsLength; i++)
        {
            dots[i] = Instantiate(_aim.dot, _sockets.projectile.position, Quaternion.identity);
            dots[i].transform.SetParent(dotsGroup.transform);
        }

        //delete old drag ui
        if (aimDragClone != null)
            Destroy(aimDragClone);

        //spawn drag ui
        aimDragClone = Instantiate(_aim.aimDragUi, new Vector3(_aim.clickPosition.x, _aim.clickPosition.y, 0), Quaternion.identity);
        aimDragClone.transform.localScale = new Vector3(1, 1, 1);

        //delete old spawn drag ui index
        if (mouseDragClone != null)
            Destroy(mouseDragClone);

        //spawn drag ui index
        mouseDragClone = Instantiate(_aim.mouseDragUi, new Vector3(_aim.clickPosition.x, _aim.clickPosition.y, 0), Quaternion.identity);
        mouseDragClone.transform.localScale = new Vector3(1, 1, 1);

        //call event
        _events.OnStartAiming.Invoke();
    }

    private void PlayerStopAiming() 
    {
        if (_aim.isAiming)
        {
            controlState = false;

            projectile = _weap.Shoot(_sockets.projectile.position, _sockets.projectile.rotation, shotPower);
            _cam.GetComponent<CameraController>().FollowProjectile(projectile);

            _aim.isAiming = false;
            _aim.mousePosition = Vector3.zero;
            _aim.clickPosition = Vector3.zero;
            _aim.aimPower = 0;
            _aim.aimAngle = 0;
            _characterRig._anim.SetBool("IsAiming", _aim.isAiming);

            //call event
            _events.OnShoot.Invoke();

            if (dotsGroup != null)
                Destroy(dotsGroup);

            if (aimDragClone != null)
                Destroy(aimDragClone);

            if (mouseDragClone != null)
                Destroy(mouseDragClone);
        }
    }

    private void PlayerAiming(Vector3 position) 
    {
        _aim.mousePosition = _cam.ScreenToWorldPoint(position);
        _aim.dragDistance = Vector3.Distance(_aim.mousePosition, _aim.clickPosition);

        //get aim direction
        direction = _aim.mousePosition - _aim.clickPosition;
        angle = Vector2.Angle(direction, Vector3.right);

        if (_aim.mousePosition.y > _aim.clickPosition.y)
            angle = -angle;

        if (angle >= 0 && angle <= 90)
            _aim.aimAngle = Mathf.Lerp(90f, 0f, (Mathf.Abs(angle) - 90f) / -90f);
        else if (angle <= 0 && angle >= -90)
            _aim.aimAngle = Mathf.Lerp(-90f, 0f, (Mathf.Abs(angle) - 90f) / -90f);
        else if (angle >= 90 && angle <= 180)
            _aim.aimAngle = Mathf.Lerp(0, 90f, (Mathf.Abs(angle) - 180f) / (90f - 180f));
        else if (angle <= -90 && angle >= -180)
            _aim.aimAngle = Mathf.Lerp(0, -90f, (Mathf.Abs(angle) - 180f) / (90f - 180f));



        //dot aim direction
        direction = _aim.dotsPivots[1].position - _aim.dotsPivots[0].position;

        //calculate aim drag power based on drag distance
        _aim.aimPower = Mathf.Lerp(100f, 0f, (_aim.dragDistance - _aim.aimMaxDragDistance) / -_aim.aimMaxDragDistance);
        shotPower = Mathf.Lerp(25f, 1f, (_aim.aimPower - 100f) / -100f);

        //set animator aim angle
        _characterRig._anim.SetFloat("AimBlend", _aim.aimAngle);

        //aim pivot
        _aim.aimPivot.localRotation = Quaternion.Euler(0, 0, _aim.aimAngle);

        //calculate facing direction based on drag direction
        if (_aim.mousePosition.x > _aim.clickPosition.x)
        {
            if (_aim.isFacingRight)
            {
                _aim.isFacingRight = false;
                transform.localScale = new Vector3(-1, 1, 1);

                _events.OnFlipLeft.Invoke();
            }
        }
        else if (_aim.mousePosition.x < _aim.clickPosition.x)
        {
            if (!_aim.isFacingRight)
            {
                _aim.isFacingRight = true;
                transform.localScale = new Vector3(1, 1, 1);

                _events.OnFlipRight.Invoke();
            }
        }

        //visualize aim dots
        for (int i = 0; i < _aim.dotsLength; i++)
            dots[i].transform.position = GetDotsPosition(_aim.dotsSpace * i);

        //update info texts
        _aim.aimPowerText.text = _aim.aimPower.ToString("0") + "%";
        _aim.aimAngleText.text = _aim.aimAngle.ToString("0") + "°";

        //update drag ui position
        mouseDragClone.transform.position = new Vector3(_aim.mousePosition.x, _aim.mousePosition.y, 0);

        //update camera zoom
        _cam.orthographicSize = Mathf.Lerp(5f, 4f, (_aim.aimPower - 100f) / -100f);
    }
#endregion


    //AI CONTROLLER
    #region REGION
    private void AiStartAiming() 
    {
        _aim.isAiming = true;

        aiAimIsReady = false;
        aiPowerIsReady = false;

        //calculate ai aiming
        AiCalculateAim();

        //setup opponent distance popup ui
        _aim.disText.text = Vector3.Distance(transform.position, _gameMng.GetOpponentTranform(ccIndex).position).ToString("0") + "M";
        SetUpdateDistanceBoxPosition();

        //spawn weapon projectile
        if (_weap.weaponType == WeaponController.WeaponType.Bow)
            _weap.SpawnProjectile(_sockets.hand_l);
        else if (_weap.weaponType == WeaponController.WeaponType.Spear)
            _weap.SpawnProjectile(_sockets.hand_r);

        //update animator variables
        _characterRig._anim.SetInteger("WeaponType", (int)_weap.weaponType);
        _characterRig._anim.SetBool("IsAiming", _aim.isAiming);

        //call event
        _events.OnActivateAiming.Invoke();

        //enable aiming state
        _aim.isAiming = true;
    }

    private void AiController()
    {
        if (!_aim.isAiming)
            return;

        //update opponents popup ui position
        SetUpdateDistanceBoxPosition();

        //update aiming main variables
        _aim.aimPower = Mathf.MoveTowards(_aim.aimPower, aiPower, Time.deltaTime * Mathf.Abs(aiPower) * .5f);
        _aim.aimAngle = Mathf.MoveTowards(_aim.aimAngle, aiAngle, Time.deltaTime * Mathf.Abs(aiAngle) * .5f);

        //calculate aim drag power based on drag distance
        shotPower = Mathf.Lerp(25f, 1f, (_aim.aimPower - 100f) / -100f);

        //set animator aim angle
        _characterRig._anim.SetFloat("AimBlend", _aim.aimAngle);

        //aim pivot
        _aim.aimPivot.localRotation = Quaternion.Euler(0, 0, _aim.aimAngle);

        //update info texts
        _aim.aimPowerText.text = _aim.aimPower.ToString("0") + "%";
        _aim.aimAngleText.text = _aim.aimAngle.ToString("0") + "°";

        //update camera zoom
        _cam.orthographicSize = Mathf.Lerp(5f, 4f, (_aim.aimPower - 100f) / -100f);

        //check is ai is ready to shoot
        if (_aim.aimPower == aiPower)
            aiPowerIsReady = true;
        if (_aim.aimAngle == aiAngle)
            aiAimIsReady = true;
        if (aiPowerIsReady && aiAimIsReady)
        {
            //diable controller state
            controlState = false;

            //shoot projectile
            projectile = _weap.Shoot(_sockets.projectile.position, _sockets.projectile.rotation, shotPower);

            //set camera to follow projectile
            _cam.GetComponent<CameraController>().FollowProjectile(projectile);

            //reset aim variables
            _aim.isAiming = false;
            _aim.mousePosition = Vector3.zero;
            _aim.clickPosition = Vector3.zero;
            _aim.aimPower = 0;
            _aim.aimAngle = 0;

            //disable aim animation
            _characterRig._anim.SetBool("IsAiming", false);

            //call event
            _events.OnShoot.Invoke();
        }
    }

    private void AiCalculateAim()
    {
        //find angle degrees
        float x = _gameMng.GetOpponentTranform(ccIndex).position.x - transform.position.x;
        x = Mathf.Abs(x);
        float y = _gameMng.GetOpponentTranform(ccIndex).position.y - transform.position.y;
        float deg = Mathf.Rad2Deg * Mathf.Atan(Mathf.Tan(y / x));

        //calculate ai power based on opponet distance
        aiPower = Mathf.Lerp(100f, 40f, (x - 60f) / -60f);

        //calculate ai aim angle based on opponet distance
        if (x <= 20f)
        {
            aiAngle = Mathf.Lerp(20f, 0f, (x - 20f) / -20f);

            if (deg > 0f)
            {
                aiAngle += deg;
                aiPower += Mathf.Lerp(10f, 0f, (deg - 45f) / -45f);
            }
            else if (deg < 0f)
            {
                aiAngle -= Mathf.Abs(deg);
                aiPower -= Mathf.Lerp(10f, 0f, (Mathf.Abs(deg) - 45f) / -45f);
            }
        }
        else if (x > 20f && x <= 40f)
        {
            aiAngle = Mathf.Lerp(28.5f, 20f, (x - 40f) / (20f - 40f));

            if (deg > 0f)
            {
                aiAngle += deg;
                aiPower += Mathf.Lerp(15f, 0f, (deg - 45f) / -45f);
            }
            else if (deg < 0f)
            {
                aiAngle -= Mathf.Abs(deg);
                aiPower -= Mathf.Lerp(15f, 0f, (Mathf.Abs(deg) - 45f) / -45f);
            }
        }
        else if (x > 40f)
        {
            aiAngle = Mathf.Lerp(35f, 28.5f, (x - 60f) / (40f - 60f));

            if (deg > 0f)
            {
                aiAngle += deg;
                aiPower += Mathf.Lerp(25f, 0f, (deg - 45f) / -45f);
            }
            else if (deg < 0f)
            {
                aiAngle -= Mathf.Abs(deg);
                aiPower -= Mathf.Lerp(25f, 0f, (Mathf.Abs(deg) - 45f) / -45f);
            }
        }

        //clamp ai power
        aiPower = Mathf.Clamp(aiPower, 0f, 100f);
    }
    #endregion


    //SET
    #region REGION
    public void SetControlState()
    {
        if (_characterRig.ragdollState)
            StartCoroutine(WaitForRagdollDelay());
        else
        {
            controlState = true;

            //update opponents popup ui position
            SetUpdateDistanceBoxPosition();

            if (_ai.isAI)
                AiStartAiming();
            else
                _events.OnActivateAiming.Invoke();
        }
    }

    public void SetCharacterSkin(Sprite[] spriteSkin)
    {
        _characterRig.sprites[0].sprite = spriteSkin[1];//body sprite
        _characterRig.sprites[1].sprite = spriteSkin[0];//head sprite
        _characterRig.sprites[2].sprite = spriteSkin[2];//arm R sprite
        _characterRig.sprites[3].sprite = spriteSkin[3];//lower R arm sprite
        _characterRig.sprites[4].sprite = spriteSkin[2];//arm L sprite
        _characterRig.sprites[5].sprite = spriteSkin[4];//lower L arm sprite
        _characterRig.sprites[6].sprite = spriteSkin[5];//thump sprite
        _characterRig.sprites[7].sprite = spriteSkin[6];//leg R sprite
        _characterRig.sprites[8].sprite = spriteSkin[7];//lower leg R sprite
        _characterRig.sprites[9].sprite = spriteSkin[6];//leg L sprite
        _characterRig.sprites[10].sprite = spriteSkin[7];//lower leg L sprite
    }

    public void SetOpponentIcon(Sprite sprite)
    {
        _aim.disCharSprite.sprite = sprite;

        if (ccIndex == 1)
            _aim.disBoxDir.localScale = new Vector3(-1, 1, 1);
    }

    public void SetCharacterWeapon(int index)
    {
        //add new weapon
        GameObject weapon = Instantiate(Resources.Load<GameObject>("Character_Weapons/CC_Weapon_" + index.ToString("000")));
        _weap = weapon.GetComponent<WeaponController>();

        if (_weap.weaponType == WeaponController.WeaponType.Bow)
            weapon.transform.SetParent(_sockets.hand_l);
        else if (_weap.weaponType == WeaponController.WeaponType.Spear)
            weapon.transform.SetParent(_sockets.hand_r);

        weapon.transform.localPosition = Vector3.zero;
        weapon.transform.localEulerAngles = Vector3.zero;
        weapon.transform.localScale = Vector3.one;
    }

    private void SetFacingDirection(bool facingRight)
    {
        _aim.isFacingRight = facingRight;

        if (_aim.isFacingRight)
        {
            transform.localScale = new Vector3(1, 1, 1);

            //call event
            _events.OnFlipRight.Invoke();
        }
        else
        {
            transform.localScale = new Vector3(-1, 1, 1);

            //call event
            _events.OnFlipLeft.Invoke();
        }
    }

    private void SetUpdateDistanceBoxPosition()
    {
        float opponentPosition_y = 0;
        float screen_x = 0;

        if (ccIndex == 0)
        {
            screen_x = (float)Screen.width - 130f;
            screen_x = _cam.ScreenToWorldPoint(new Vector2(screen_x, 0)).x;
        }
        else
        {
            screen_x = 130f;
            screen_x = _cam.ScreenToWorldPoint(new Vector2(screen_x, 0)).x;
        }
        opponentPosition_y = _gameMng.GetOpponentTranform(ccIndex).position.y;
        _aim.disBox.transform.position = new Vector3(screen_x, opponentPosition_y, 0);
    }
    #endregion


    //GET
    #region REGION
    private bool GetScreenAimBounds(Vector2 screenPosition) 
    {
        float xPerc = GetPercentageValue((float)Screen.width, 20f);
        float yPerc = GetPercentageValue((float)Screen.height, 20f);

        if (screenPosition.x >= xPerc &&
           screenPosition.x <= (float)Screen.width - xPerc &&
           screenPosition.y >= yPerc &&
           screenPosition.y <= (float)Screen.height - yPerc
        )
            return true;
        else
            return false;
    }

    private float GetPercentageValue(float value, float percentage) 
    {
        return value * percentage / 100f;
    }

    private Vector2 GetDotsPosition(float t)
    {
        Vector2 position = (Vector2)_sockets.projectile.position + (direction.normalized * shotPower * t) + .55f * Physics2D.gravity * (t * t);
        return position;
    }
    #endregion


    //RAGDOLL SYSTEM
    #region REGION
    public void SetDamage(float damage, float power, Rigidbody2D hitCol, Vector3 projectilePos, Vector3 colPos)
    {
        _characterRig._anim.enabled = false;
        _characterRig.ragdollState = true;

        foreach (Rigidbody2D rig in rd_rigs)
        {
            rd_initPos[rig] = rig.transform.localPosition;
            rd_initRot[rig] = rig.transform.localRotation;
            rig.bodyType = RigidbodyType2D.Dynamic;
        }

        foreach (HingeJoint2D joint in rd_joints)
        {
            joint.enabled = true;
        }

        var direction = (projectilePos - colPos).normalized;
        var magnitude = (power * 250f) * Vector2.Dot(direction, Vector2.up);

        hitCol.AddForceAtPosition(-direction * magnitude, colPos);

        string hitPart = hitCol.gameObject.name;
        if (hitPart == "_HEAD")
        {
            health -= 50;
        }
        else if (hitPart == "_BODY")
        {
            health -= 35;
        }
        else
        {
            health -= 20;
        }

        if (health < 0)
            health = 0;

        _gameMng.SetHealthDamage(ccIndex, health);
    }

    private void DeactivateRagdoll() 
    {
        Vector3 newPos = rd_rigs[0].transform.position;
        RaycastHit2D hit = Physics2D.Raycast(newPos, -Vector2.up, 100f, _characterRig.ragdollMask);
        newPos.y = hit.point.y + .9f;
        transform.position = newPos;

        _characterRig._anim.enabled = true;
        _characterRig.ragdollState = false;

        foreach (Rigidbody2D rig in rd_rigs)
        {
            rig.velocity = Vector2.zero;
            rig.angularVelocity = 0f;
            rig.bodyType = RigidbodyType2D.Kinematic;
            rig.transform.localPosition = rd_initPos[rig];
            rig.transform.localRotation = rd_initRot[rig];
        }

        foreach (HingeJoint2D joint in rd_joints)
        {
            joint.enabled = false;
        }
    }

    IEnumerator WaitForRagdollDelay()
    {
        yield return new WaitForSeconds(2);

        DeactivateRagdoll();

        controlState = true;

        //update opponents popup ui position
        SetUpdateDistanceBoxPosition();

        if (_ai.isAI)
            AiStartAiming();
        else
            _events.OnActivateAiming.Invoke();
    }
    #endregion
}
