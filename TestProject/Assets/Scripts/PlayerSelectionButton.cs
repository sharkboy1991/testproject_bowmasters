﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayerSelectionButton : MonoBehaviour
{
    private Canvas_PlayerSelection _cvsManager;

    public int buttonIndex;
    public Image board;
    public Image icon;

    [Space(25)]
    public UnityEvent onClick;

    public void InitButton(Canvas_PlayerSelection canvasManager,int index, Sprite sprite) 
    {
        _cvsManager = canvasManager;
        buttonIndex = index;
        icon.sprite = sprite;
    }

    public void SetBoardColor(Color color) 
    {
        board.color = color;
    }

    public void OnClick()
    {
        _cvsManager.SelectPlayer(buttonIndex);
        onClick.Invoke();
    }
}
