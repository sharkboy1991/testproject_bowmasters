﻿using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //VARIABLES
    #region REGION
    [Header("CANVAS MANAGER")]
    public CanvasManager _cvsMng;

    [Header("CHARACTER PREFAB")]
    public GameObject charPrefab;

    [Header ("CHARACTER POSITIONS")]
    public Transform p1Pos;
    public Transform p2Pos;
    private Transform p1Tfm;
    private Transform p2Tfm;

    [Header("CAMERA CONTROLLER")]
    public CameraController _camCtrl;

    [Header("ATTACK")]
    public AttackState _roundState;
    public enum AttackState {PlayerRound, EnemyRound }

    private Sprite[] sprite;

    private CharacterController[] _cC = new CharacterController[2];
    public CharacterController PlayerController(int index)
    {
        return _cC[index];
    }
    #endregion


    //MAIN METHODS
    #region
    private void Start()
    {
        p1Tfm = SpawnCharacter(0, p1Pos.position, true, false);//spawn player
        p2Tfm = SpawnCharacter(1, p2Pos.position, false, true);//spawn enemy

        _camCtrl.InitCameraController(this, p1Tfm, p2Tfm); //initialize camera controller

        int playerSkin = PlayerPrefs.GetInt("PlayerSkinSelection");//load player selected skin
        int enemySkin = Random.Range(0, 4);//get random enemy skin

        for (int i = 0; i < 2; i++)
        {
            //update healthbar uis
            SetHealthDamage(i, 100f);

            //update skins and character icons
            if (i == 0)
            {
                _cC[i].SetOpponentIcon(GetCharacterPartSprite(enemySkin, 0));
                sprite = GetCharacterSprites(playerSkin);
                _cC[i].SetCharacterWeapon(playerSkin);
            }
            else if (i == 1)
            {
                _cC[i].SetOpponentIcon(GetCharacterPartSprite(playerSkin, 0));
                sprite = GetCharacterSprites(enemySkin);
                _cC[i].SetCharacterWeapon(enemySkin);
            }
            _cC[i].SetCharacterSkin(sprite);
            _cvsMng.SetIcon(i, sprite[0]);
        }
    }

    private void OnDrawGizmos()
    {
        GUIStyle gui = new GUIStyle();
        gui.alignment = TextAnchor.MiddleCenter;
        gui.normal.textColor = Color.green;
        gui.fontStyle = FontStyle.Bold;

        Gizmos.DrawWireCube(p1Pos.position, new Vector3(1,1.8f,0));
        Gizmos.DrawWireCube(p2Pos.position, new Vector3(1, 1.8f, 0));
    }
    #endregion


    //SET
    #region
    public void SetHealthDamage(int index, float health)
    {
        _cvsMng.SetHealth(index, health);
    }
    #endregion


    //GET
    #region
    private Sprite[] GetCharacterSprites(int skinIndex)
    {
        return Resources.LoadAll<Sprite>("Character_Skins/CC_Skin_" + skinIndex);
    }

    private Sprite GetCharacterPartSprite(int skinIndex, int bodyPartIndex)
    {
        return Resources.LoadAll<Sprite>("Character_Skins/CC_Skin_" + skinIndex)[bodyPartIndex];
    }

    public Transform GetOpponentTranform(int index)
    {
        if (index == 0)
            return p2Tfm;
        else
            return p1Tfm;
    }
    #endregion


    //OTHERS
    public void HandleRound(bool attackState)
    {
        if (_roundState == AttackState.PlayerRound)
        {
            if (attackState)
            {
                _roundState = AttackState.EnemyRound;
                _cC[1].SetControlState();
            }
        }
        else if (_roundState == AttackState.EnemyRound)
        {
            if (attackState)
            {
                _roundState = AttackState.PlayerRound;
                _cC[0].SetControlState();
            }
        }
    }

    private Transform SpawnCharacter(int controllerIndex, Vector3 spawnPos, bool facingRight, bool isAi)
    {
        //spawn character controller
        GameObject clone = Instantiate(charPrefab, spawnPos, Quaternion.identity);
        clone.transform.localScale = Vector3.one;

        _cC[controllerIndex] = clone.GetComponent<CharacterController>();
        _cC[controllerIndex].InitCharacterController(controllerIndex, this, facingRight, isAi);

        return clone.transform;
    }

    public void OnProjectileHit()
    {
        StartCoroutine(Delay());
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(1.5f);

        if (_roundState == AttackState.PlayerRound)
        {
            _camCtrl.FollowEnemy();
        }
        else if (_roundState == AttackState.EnemyRound)
        {
            _camCtrl.FollowPlayer();
        }
    }
}
