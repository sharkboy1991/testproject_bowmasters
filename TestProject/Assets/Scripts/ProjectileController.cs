﻿using UnityEngine;
using UnityEngine.Events;

public class ProjectileController : MonoBehaviour
{
    public Transform forcePosition;
    public SpriteRenderer projectileRenderer;
    private bool activeTrigger = true;
    private Rigidbody2D _rig;
    private bool shootState = false;
    private float power;
    private float damage;

    [Header("EVENTS")]
    public UnityEvent onHitCharacter;
    public UnityEvent onHitGround;



    public void SetProjectileSprite(Sprite sprite) 
    {
        projectileRenderer.sprite = sprite;
    }

    public void ShootProjectile(float power, float damage)
    {
        _rig = GetComponent<Rigidbody2D>();
        _rig.isKinematic = false;
        _rig.gravityScale = 1;
        _rig.velocity = -transform.up * power;
        GetComponent<Collider2D>().enabled = true;

        this.power = power;
        this.damage = damage;
        shootState = true;
    }

    private void Update()
    {
        if (shootState)
        {
            float angle = Mathf.Atan2(_rig.velocity.y, _rig.velocity.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle + 90f, Vector3.forward);
        }
    }

    private void OnTriggerEnter2D (Collider2D other)
    {
        if (!activeTrigger)
            return;

        if (other.transform != null)
        {
            shootState = false;
            activeTrigger = false;

            if (other.transform.root.GetComponent<CharacterController>() != null)
            {
                transform.SetParent(other.transform);
                other.transform.root.GetComponent<CharacterController>().SetDamage(damage, power, other.GetComponent<Rigidbody2D>(), forcePosition.position, other.transform.position);

                onHitCharacter.Invoke();
            }
            else
            {
                onHitGround.Invoke();
            }

            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().OnProjectileHit();

            Destroy(_rig);
            Destroy(GetComponent<Collider2D>());
            Destroy(gameObject, 4);
        }
    }
}
